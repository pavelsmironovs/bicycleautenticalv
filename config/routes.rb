Rails.application.routes.draw do
  root to: 'home#index'  # change to new syntax, use resources
  get 'home/index'
  get '/edit_reservations/edit/:id' => 'bicycles#edit_reservation'
  get 'bicycles/reservations_list' => 'bicycles#list', as: 'reservations_list'
  get 'bicycles/edit_reservations' => 'bicycles#edit_reservations_list', as: 'edit_reservations'
  patch 'bicycles/update' => 'bicycles#update_reservation', as: 'update_reservations_path'
  get 'bicycles/update_reservation'
  get 'bicycles/reserve' => 'bicycles#reserve', as: 'new_bicycle_event'
  get 'bicycles/reserve_for' => 'bicycles#reserve_for', as: 'new_bicycle_event_for'
  post 'bicycles/reserve/submit' => 'bicycles#create_reservation', as: 'create_bicycle_event'
  post 'bicycles/reserve_for/submit' => 'bicycles#create_reservation_for', as: 'create_bicycle_event_for'
  get 'bicycle/reserve_for'
  get 'bicycles/notice' => 'bicycles#notice', as: 'new_bicycle_event_notice'
  get 'employees/index' => 'employees#index', as: 'employees'
  get 'employees/new' => 'employees#new', as: 'new_employee'
  post 'employees/create' => 'employees#create', as: 'create_employee'
  get 'employees/edit/:id' => 'employees#edit'
  patch 'employees/edit' => 'employees#update'
  get 'employees/reserve'
  get 'employees/update'
  get 'employees/notice'
  devise_scope :employee do
    get 'sign_out', to: 'devise/sessions#destroy', as: 'sign_out'
  end

  devise_for :employees, path: '/'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
