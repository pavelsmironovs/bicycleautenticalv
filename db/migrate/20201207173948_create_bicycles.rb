class CreateBicycles < ActiveRecord::Migration[6.0]
  def change
    create_table :bicycles do |t|
      t.string :model
      t.datetime :usage_start
      t.datetime :usage_end
      t.integer :employee_id

      t.timestamps
    end
  end
end
