class CreateBicycleEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :bicycle_events do |t|
      t.datetime :started
      t.datetime :finished
      t.integer :bicycle_id
      t.integer :employee_id

      t.timestamps
    end
  end
end
