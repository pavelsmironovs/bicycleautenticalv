class AddImageToBicycles < ActiveRecord::Migration[6.0]
  def change
    add_column :bicycles, :image, :string
  end
end
