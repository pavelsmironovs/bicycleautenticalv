class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :encrypted_password
      t.string :image

      t.timestamps
    end
  end
end
