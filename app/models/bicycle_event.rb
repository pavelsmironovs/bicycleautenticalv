class BicycleEvent < ApplicationRecord
  belongs_to :bicycle
  belongs_to :employee
  validates_presence_of :started, :finished
  validates_uniqueness_of :started, scope: :bicycle_id


  def bike_taken?
    (started..finished).include? Time.now
  end
end
