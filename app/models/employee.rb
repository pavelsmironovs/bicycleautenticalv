class Employee < ApplicationRecord
  has_many :bicycle_events
  devise :database_authenticatable, :registerable
  validates_presence_of :name, :surname, :email, :encrypted_password
  validates_uniqueness_of :email

  def is_admin?
    is_admin == true
  end
end
