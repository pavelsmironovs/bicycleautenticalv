class Bicycle < ApplicationRecord
  has_many :bicycle_events
  validates_presence_of :model
end
