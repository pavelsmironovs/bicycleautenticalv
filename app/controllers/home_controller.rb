class HomeController < ApplicationController
  before_action :authenticate_employee!
  def index
    @bicycles = Bicycle.all
  end
end

