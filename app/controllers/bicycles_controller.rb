class BicyclesController < ApplicationController
  before_action :authenticate_employee!
  def new; end

  def create; end

  def update; end

  def list
    @bicycles = Bicycle.all
    @events = BicycleEvent.all
  end

  def reserve
    @bicycles = Bicycle.all
    @event = BicycleEvent.new
    @events = BicycleEvent.all
  end

  def reserve_for
    @employees = Employee.all
    @bicycles = Bicycle.all
    @event = BicycleEvent.new
    @events = BicycleEvent.all
  end

  def create_reservation_for
    @event = Bicycle.where(id: event_params[:bicycle_id].to_i)
      .first
      .bicycle_events
      .new(event_params)
    @events = BicycleEvent.where(bicycle_id: @event.bicycle_id)
    @event.save unless check_event_availability(@events, event_params) == false
    render 'notice'
  end

  def create_reservation
    @event = Bicycle.where(id: event_params[:bicycle_id].to_i)
      .first
      .bicycle_events
      .new(event_params.merge(employee_id: current_employee.id))
    @events = BicycleEvent.where(bicycle_id: @event.bicycle_id)
    @event.save if (check_event_availability(@events, event_params) == false)
    render 'notice'
  end

  def edit_reservations_list
    @events = BicycleEvent.all
    @employees = Employee.all
  end

  def edit_reservation
    @event = BicycleEvent.find(params[:id])
    @employees = Employee.all
  end

  def update_reservation
    @event = BicycleEvent.find(event_params[:id])
    events = BicycleEvent.where(bicycle_id: @event.bicycle_id)
    if check_event_availability(events, event_params) == false
      flash[:notice] = 'Event not updated - it overlaps with other event for this bicycle'
      redirect_to edit_reservations_path
    else
      @event.update(event_params)
      flash[:notice] = 'Event sucessfully updated'
      redirect_to edit_reservations_path
    end
  end

  private

  def event_params
    params.require(:bicycle_event).permit(:id, :started, :finished, :bicycle_id, :employee_id)
  end

  def check_event_availability(events, event_params)
    possible_event = BicycleEvent.new(event_params)
    possible_event_times = possible_event.started..possible_event.finished
    events.each do |event|
      time_available = event.started..event.finished
      if time_available.include? possible_event_times
        break
        return false
      else
        return true
      end
    end
  end
end
