class EmployeesController < ApplicationController
  before_action :authenticate_employee!
  before_action :is_admin?

  def index
    @employees = Employee.all
  end

  def new
    @employee = Employee.new
  end

  def create
    pass = BCrypt::Password.create(employee_params[:encrypted_password])
    @employee = Employee.new(employee_params.merge(encrypted_password: pass))
    @employee.save
    render 'notice'
  end

  def edit
    @employee = Employee.find(params[:id])
  end

  def update
    @employee = Employee.find(params[:employee][:id])
    @employee.update(employee_params.except(:encrypted_password, :id))
    flash[:notice] = 'Employee updated'
    flash[:notice] = 'Update failed' unless @employee.update(employee_params.except(:encrypted_password, :id))
    redirect_to employees_path
  end

  private

  def employee_params
    params.require(:employee).permit(:id, :name, :surname, :email, :encrypted_password)
  end

  def is_admin?
    redirect_to root_path unless current_employee.is_admin?
  end
end
